import requests
import json


class IssueRequest:
    itopconnection = {}
    json_data = ""

    def run(self):
        if not self.itopconnection.get("url"):
            raise Exception("Please provide at least a base url for the iTop server!")

        if not self.itopconnection.get("skip_ssl"):
            self.itopconnection["skip_ssl"] = False
        if not self.itopconnection.get("api_version"):
            self.itopconnection["api_version"] = "1.3"

        operation = json.loads(self.json_data).get("operation")
        url = f"{self.itopconnection['url']}/webservices/rest.php?version={self.itopconnection['api_version']}&login_mode=basic"

        print(f"Issuing iTop API request to {url}")
        print(f"Operation: {operation}")

        r = requests.post(
            url,
            verify=not self.itopconnection["skip_ssl"],
            auth=(self.itopconnection.get("user"), self.itopconnection.get("password")),
            data={
                "json_data": self.json_data,
            },
        )
        result = json.loads(r.text)

        if r.status_code >= 400:
            raise Exception(f"Error - iTop Server responded with HTTP {r.status_code}")

        if result["code"] != 0:
            raise Exception(f"{result['code']}: {result['message']}")

        print(f"Successfully received API response.")
        return result